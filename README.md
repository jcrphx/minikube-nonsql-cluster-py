# Overview

This demo has been created in response to a challenge task for a Deployment Engineer position


Build a message stream system using Kafka and MongoDB backend as data sink.

* Build the necessary manifests to deploy Kafka and MongoDB in Kubernetes
* List the configuration steps needed to use MongoDB as the sink of data coming from Kafka
* Create a python script that streams a dictionary with fields: Shopper ID (random UUID), Session ID (random UUID), and timestamp to a Kafka cluster
* Create a python script that connects to the Kafka cluster and retrieves a stream of messages from a start to end timestamp. You should be able to repeat this previous step multiple times as MongoDB backs the data request

# Toolset
* OS Platform: Windows 10
* Kubernetes distro: minikube v1.25
* Python: 3.10.1